# Introduction to ML for HEP


Lecture course given @LPI for 3rd year MIPT students in the spring semester 2019.

**Synopsis:**
1.  Supervised/Unsupervised/RL, linear models, trees, metrics, overfitting, regularization, CV
2.  Ensembles (Random Forest, BDT), Neural Networks, CNN, RNN
3.  Modern applications in HEP


**Gateway to an ML world (to name a few):**

* [HEP Community White Paper](https://arxiv.org/abs/1807.02876) - status overview of ML in HEP: what should be done, why and how

* [IML @LHC](https://iml.web.cern.ch) - community of data scientists at LHC. They do open meeting each month!

* [Summer School on ML in HEP by HSExYandex](https://indico.cern.ch/event/687473/)

* [MIPTxYandex specialisation on Coursera](https://www.coursera.org/specializations/machine-learning-data-analysis) - A must for beginners (imho). Free certificate for MIPT students (just send an email to mooc@phystech.edu), free enrollment for everyone

* [Data Mining in Action](https://github.com/data-mining-in-action/) - ML course by Viktor Kantor (Yandex.Taxi) and co. Utterly amazing and highly recommended - just pick the track you like

* [Deep Learning course book](http://www.deeplearningbook.org/) - a very broad and detailed explaination of DL

* [DL course in Yandex School of Data Analysis](https://github.com/yandexdataschool/Practical_DL) - if you feel like you know the basics and want to go deeper
 
* [Yandex School of Data Analysis](https://github.com/yandexdataschool/) - or more generally

* [dlschool](https://www.dlschool.org/) - proper course by MIPT students on ML+DL

* [LOTS OF LINKS](https://github.com/demidovakatya/vvedenie-mashinnoe-obuchenie) - actually I could've put here just this one
 
* [LOTS OF MODELS](https://www.kaggle.com/shivamb/data-science-glossary-on-kaggle) - enormous summary-kernel 

* [CNN from Stanford](http://cs231n.stanford.edu/) - probably a must on NN and CNN basics

* [fast.ai](https://www.fast.ai/) - more on nets

* [articles on Medium](https://medium.com/topic/machine-learning) - but beware, there are at times not so good ones

* links in the slides from this repo

* and way more on the Internet (just google it in English)